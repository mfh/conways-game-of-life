import sys
from numpy import *
import pygame
pygame.init() 

#create the screen and board
bwidth=640/10
bheight=280/10
window = pygame.display.set_mode((bwidth*10, bheight*10)) 
board = zeros((bwidth,bheight))


Clock = pygame.time.Clock()
local_neighbors=(array([-1,1]), \
			array([0,1]), \
			array([1,1]), \
			array([-1,0]), \
			array([1,0]), \
			array([-1,-1]), \
			array([0,-1]), \
			array([1,-1]), \
			)
def living(coords):
	# check if coord is valid and living
	x,y=coords
	if x < 0 or x >= bwidth or y < 0 or y >= bheight:
		return False
	if board[x][y]==0:
		return False
	return True

def get_neighbors(x,y):
	neighbors=[]
	coords = array([x,y])
	for l in local_neighbors:
			neighbors.append(coords+l)
	return filter(living,neighbors)
	

started=False
print "Click or drag to set or remove squares.\nPress enter to start, pause, and unpause. Pausing enables the grid.\nPress delete while paused to clear the board.\nPress Q to quit."
while True: 
	#tickFPS = Clock.tick(30)
	for event in pygame.event.get(): 
		if event.type == pygame.QUIT: 
			sys.exit(0) 
		elif event.type==pygame.MOUSEBUTTONDOWN:
			x,y = pygame.mouse.get_pos()
			board[x/10][y/10] = 1 if board[x/10][y/10]==0 else 0
		elif event.type==pygame.MOUSEMOTION and 1 in pygame.mouse.get_pressed():
			x,y = pygame.mouse.get_pos()
			board[x/10][y/10] = 1
		elif event.type==pygame.KEYUP:
			if event.key==pygame.K_RETURN:
				started=not started
			elif event.key==pygame.K_DELETE and not started:
				board=zeros((bwidth,bheight))
			elif event.key==pygame.K_q:
				sys.exit(0)
	
	if started:
		newboard=zeros((bwidth,bheight))
		pygame.draw.rect(window, (0,0,0), (0,0,bwidth*10,bheight*10),0)
		for x in range(bwidth):
			for y in range(bheight):
				alive_neighbors=get_neighbors(x,y)
				if board[x][y]==1 and len(alive_neighbors) < 2:
					""" Case 1
					Any live cell with fewer than two live neighbors dies, as if caused by under-population
					"""
					newboard[x][y]=0
			#		pygame.draw.rect(window, (25,25,25), (x*10,y*10,10,10),1)
				elif board[x][y]==1 and len(alive_neighbors) >=2 and len(alive_neighbors) <=3:
					""" Case 2
					Any live cell with two or three live neighbors lives on to the next generation
					"""
					newboard[x][y]=1
					pygame.draw.rect(window, (255,255,255), (x*10,y*10,10,10))

				elif board[x][y]==1 and len(alive_neighbors) > 3:
					""" Case 3
					Any live cell with more than three live neighbors dies, as if by overcrowding.
					"""
					newboard[x][y]=0
			#		pygame.draw.rect(window, (25,25,25), (x*10,y*10,10,10),1)
				elif board[x][y]==0 and len(alive_neighbors) == 3:
					""" Case 4
					Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction
					"""
					newboard[x][y]=1
					pygame.draw.rect(window, (255,255,255), (x*10,y*10,10,10))
			#	else:
			#		pygame.draw.rect(window, (25,25,25), (x*10,y*10,10,10),1)
		board=newboard

	else:
		pygame.draw.rect(window, (0,0,0), (0,0,bwidth*10,bheight*10),0)
		for x in range(bwidth):
			for y in range(bheight):
				if board[x][y] == 1:
					pygame.draw.rect(window, (255,255,255), (x*10,y*10,10,10))
				else:
					pygame.draw.rect(window, (25,25,25), (x*10,y*10,10,10),1)

	#draw it to the screen
	pygame.display.flip() 
